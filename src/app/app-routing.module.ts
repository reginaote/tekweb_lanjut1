import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { PengirimComponent } from './pages/pengirim/pengirim.component';
import { TelerPosComponent6 } from './pages/teller-pos/teller-pos.component';
import { KepalaGudangComponent } from './pages/kepala-gudang/kepala-gudang.component';
import { TukangPosComponent } from './pages/tukang-pos/tukang-pos.component';
import { PenerimaComponent } from './pages/penerima/penerima.component';

const routes: Routes = [
{
path:'home',
component:HomeComponent
},
{
path:'pengirim',
component:PengirimComponent
},
{
path:'tellerPos',
component:TelerPosComponent6
},
{
path:'kepalaGudang',
component:KepalaGudangComponent
},
{
path:'tukangPos',
component:TukangPosComponent
},
{
path:'penerima',
component:PenerimaComponent
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
