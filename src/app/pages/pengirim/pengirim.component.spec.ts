import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PengirimComponent } from './pengirim.component';

describe('PengirimComponent', () => {
  let component: PengirimComponent;
  let fixture: ComponentFixture<PengirimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengirimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PengirimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
