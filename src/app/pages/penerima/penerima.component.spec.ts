import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenerimaComponent } from './penerima.component';

describe('PenerimaComponent', () => {
  let component: PenerimaComponent;
  let fixture: ComponentFixture<PenerimaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenerimaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenerimaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
