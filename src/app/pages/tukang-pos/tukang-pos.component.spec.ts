import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TukangPosComponent } from './tukang-pos.component';

describe('TukangPosComponent', () => {
  let component: TukangPosComponent;
  let fixture: ComponentFixture<TukangPosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TukangPosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TukangPosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
