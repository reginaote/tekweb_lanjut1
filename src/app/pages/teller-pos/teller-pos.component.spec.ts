import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TellerPosComponent } from './teller-pos.component';

describe('TellerPosComponent', () => {
  let component: TellerPosComponent;
  let fixture: ComponentFixture<TellerPosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TellerPosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TellerPosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
