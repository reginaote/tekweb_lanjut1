import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KepalaGudangComponent } from './kepala-gudang.component';

describe('KepalaGudangComponent', () => {
  let component: KepalaGudangComponent;
  let fixture: ComponentFixture<KepalaGudangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KepalaGudangComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KepalaGudangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
