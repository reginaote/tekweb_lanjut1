import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { PengirimComponent } from './pages/pengirim/pengirim.component';
import { TelerPosComponent6 } from './pages/teller-pos/teller-pos.component';
import { KepalaGudangComponent } from './pages/kepala-gudang/kepala-gudang.component';
import { TukangPosComponent } from './pages/tukang-pos/tukang-pos.component';
import { PenerimaComponent } from './pages/penerima/penerima.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PengirimComponent,
    TelerPosComponent6,
    KepalaGudangComponent,
    TukangPosComponent,
    PenerimaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
